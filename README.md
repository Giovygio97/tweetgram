# Tweetgram

This app is a test app for using Twitter standard API into an iOS app.
Make sure to have a functional access token given by twitter developer portal 
(link: https://developer.twitter.com/en) for testing the application.